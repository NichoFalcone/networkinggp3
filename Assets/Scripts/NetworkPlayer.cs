﻿using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayer : NetworkBehaviour
{
    [SerializeField]
    private float m_speed;
    [SerializeField]
    private Material m_localMaterial;
    [SerializeField]
    private Material m_remoteMaterial;
    private MeshRenderer m_meshRender;

    private void Awake()
    {
        m_meshRender = GetComponent<MeshRenderer>();
        m_meshRender.material = m_remoteMaterial;
    }

    /// <summary>
    /// Executed only on local player
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        m_meshRender.material = m_localMaterial;
    }

    private void Update()
    {
        ///check if is the local player
        if (isLocalPlayer)
        {
            Movement();
        }
    }

    /// <summary>
    /// Player movement logic here
    /// </summary>
    private void Movement()
    {
        float h = Input.GetAxis("Horizontal") * m_speed;
        float v = Input.GetAxis("Vertical") * m_speed;
        transform.Translate(new Vector3(h, 0, v));
    }
}
