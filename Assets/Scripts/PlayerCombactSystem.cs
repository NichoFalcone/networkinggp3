﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerCombactSystem : NetworkBehaviour
{
    [SerializeField]
    private GameObject m_bullet;

    private void Update()
    {
        if (!isLocalPlayer)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CmdShot();
        }
    }

    [Command]
    private void CmdShot()
    {
        GameObject current = Instantiate(m_bullet, transform.position, Quaternion.identity);
        NetworkServer.Spawn(current);
    }
}
